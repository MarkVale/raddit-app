<?php

namespace Raddit\AppBundle\Controller;

use Doctrine\ORM\EntityManager;
use Raddit\AppBundle\Entity\User;
use Raddit\AppBundle\Form\RequestPasswordResetType;
use Raddit\AppBundle\Form\UserType;
use Raddit\AppBundle\Mailer\ResetPasswordMailer;
use Raddit\AppBundle\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class ResetPasswordController extends Controller {
    /**
     * @param Request             $request
     * @param UserRepository      $ur
     * @param ResetPasswordMailer $mailer
     *
     * @return Response
     */
    public function requestResetAction(Request $request, UserRepository $ur, ResetPasswordMailer $mailer) {
        if (!$this->getParameter('env(NO_REPLY_ADDRESS)')) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(RequestPasswordResetType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->getData()->getEmail();

            // TODO - this is susceptible to timing attacks.
            // TODO - send only one email with all the links.
            foreach ($ur->lookUpByEmail($email) as $user) {
                $mailer->mail($user, $request);
            }

            $this->addFlash('success', 'flash.reset_password_email_sent');

            return $this->redirectToRoute('raddit_app_front');
        }

        return $this->render('@RadditApp/request_password_reset.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @ParamConverter("expires", options={"format": "U"})
     *
     * @param Request             $request
     * @param EntityManager       $em
     * @param User                $user
     * @param ResetPasswordMailer $mailer
     * @param \DateTime           $expires
     * @param string              $checksum
     *
     * @return Response
     */
    public function resetAction(
        Request $request, EntityManager $em, User $user,
        ResetPasswordMailer $mailer, \DateTime $expires, string $checksum
    ) {
        if (!$mailer->validateChecksum($checksum, $user, $expires)) {
            throw $this->createNotFoundException('Invalid checksum');
        }

        if (new \DateTime('@'.time()) > $expires) {
            throw $this->createNotFoundException('The link has expired');
        }

        $form = $this->createForm(UserType::class, $user);

        try {
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $em->flush();

                $this->addFlash('success', 'flash.user_password_updated');

                return $this->redirectToRoute('raddit_app_front');
            }
        } finally {
            $em->refresh($user);
        }

        return $this->render('@RadditApp/reset_password.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
